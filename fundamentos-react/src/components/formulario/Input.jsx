import './Input.css'
import React, { useState } from 'react'

export default props => {
    const [valor, setValor] = useState('Incial')

    function quandoMudar(e) {
        setValor(e.target.value)
    }

    return (
        <div className="Input">
            <h2> {valor} </h2>
            <div style={{
                display: 'flex',
                flexDirection: 'column'
            }}>
                <input value={valor} onChange={quandoMudar} />  {/* componente controlado */}
                <input value={valor} readOnly />
                <input value={undefined} /> {/* componente não controlado */}
            </div>
        </div>
    )
}