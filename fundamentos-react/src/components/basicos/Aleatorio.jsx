import React from 'react'

export default (props) => {
    // const min = props.min
    // const max = props.max
    // Exemplo de desetruturação
    // const [a, b] = [1, 2]
    // const { c, d, x } = { c: 12, d: 45, x: 'teste' }

    const { min, max } = props // operador destructuring (desestruturação)
    const aleatorio = parseInt(Math.random() * (max - min)) + min
    return (
        <div>
            <h2>Gerando Números Aleatórios Entre Dois Valores</h2>
            <p><strong>Valor Mínimo </strong>{min}</p>
            <p><strong>Valor Máximo </strong>{max}</p>
            <p><strong>Valor Escolhido </strong>{aleatorio}</p>
        </div>
    )
}