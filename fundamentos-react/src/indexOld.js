import './index.css'
import ReactDom from 'react-dom'
import React from 'react'

import Primeiro from './components/basicos/Primeiro'
import ComParametro from './components/basicos/ComParametro'

// 1 maneira de renderizar uma aplicação
const el = document.getElementById('root')
ReactDom.render('Olá React!', el)

// 2 maneira de renderizar uma aplicação
ReactDom.render(
    'Olá React!', 
    document.getElementById('root')
)

// 3 maneira de renderizar uma aplicação com jsx
ReactDom.render(
    <div>
        <strong>Olá React!</strong>
    </div>, 
    document.getElementById('root')
)

// 4 maneira de renderizar uma aplicação com jsx
const tag = <strong>Olá React!</strong>
ReactDom.render(
    <div>
        { tag }
    </div>, 
    document.getElementById('root')
)

// Trabalhando com componente
ReactDom.render(
    <div id="app">
        <Primeiro></Primeiro>
        <ComParametro 
            titulo="Situação do Aluno" 
            aluno="Pedro" 
            nota={9.3}    
        />
         <ComParametro 
            titulo="Situação do Aluno" 
            aluno="Maria" 
            nota={9.}    
        />
    </div>, 
    document.getElementById('root')
)