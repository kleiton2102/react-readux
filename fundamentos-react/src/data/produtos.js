export default [
    { id:1, nome: 'Nescau', preco: 5.99 },
    { id:2, nome: 'Detergente', preco: 1.99 },
    { id:3, nome: 'Macarrão', preco: 2.98 },
    { id:4, nome: 'Arroz', preco: 25.99 },
    { id:5, nome: 'Feijão', preco: 6.99 },
    { id:6, nome: 'Óleo', preco: 7.99 },
    { id:7, nome: 'Molho de Tomate', preco: 2.59 },
    { id:8, nome: 'Frango', preco: 8.99 },
    { id:9, nome: 'Papel Higiênico', preco: 14.99 },
    { id:10, nome: 'Atum', preco: 4.69 }
]